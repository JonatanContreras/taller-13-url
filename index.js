import express from 'express';
import get_place from './get_place.js';


const app = express();

let puerto = 3005;

//Definir ruta

app.get("/get_place", (req, res) => {
    res.send(get_place(req));
});

// Iniciar un servidor

app.listen(puerto, () => { console.log("Esta funcionando el servidor") });